using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NextLevelTrigger : MonoBehaviour
{
    [SerializeField] private Transform nextLevelPosition;

    private void OnTriggerEnter(Collider other)
    {
        other.gameObject.transform.position = nextLevelPosition.position;
    }
}
